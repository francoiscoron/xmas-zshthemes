PROMPT="%(?:%{$fg_bold[yellow]%}🎅 ➜:%{$fg_bold[red]%} 🎁 ➜ )"
PROMPT+=' %{$fg[cyan]%}%c%{$reset_color%} $(git_prompt_info)'

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[blue]%}git:%{$fg_bold[yellow]%}("
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg_bold[yellow]%}%{$fg_bold[red]%}✗%{$fg_bold[yellow]%}) ☃️ "
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg_bold[yellow]%}%{$fg_bold[green]%}✔︎%{$fg_bold[yellow]%}) ⛄️ "