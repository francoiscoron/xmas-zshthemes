# Xmas Zsh Theme #

Xmas zsh prompt with git status

### Install ###

* Get ZSH https://github.com/robbyrussell/oh-my-zsh
* Clone the repository
* Run

### Command ###

Move the theme in the zsh themes folder:
```
$ cp xmas.zsh-theme ~/.oh-my-zsh/themes/
```

Then edit your `.zshrc` config:
```
$ vim ~/.zshrc
```
And set the theme:
```
ZSH_THEME="xmas"
```